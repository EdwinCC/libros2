package com.example.sony.proyecto;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;

public class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder> {
    ArrayList<Item> items = new ArrayList<Item>();

    ViewGroup parent;
    public Adapter (ArrayList<Item> it) {items  = it;}


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        this.parent = parent;
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Item current = items.get(position);
        holder.userPicture.setImageResource(current.getUserPictureId());
        holder.userName.setText(current.getUserName());

        holder.bTrade.setTag(position);
        holder.bTrade.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Context c = parent.getContext();
                int i = Integer.parseInt(""+v.getTag());
                Intent intent = new Intent(c, TradeBook.class );
                intent.putExtra("userID",""+items.get(i).getUserId());
                intent.putExtra("user",BookDescription.currentUser);
                c.startActivity(intent);
            }
        });

        holder.bProfile.setTag(position);
        holder.bProfile.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Context c = parent.getContext();
                //int i = Integer.parseInt(""+v.getTag());
                Intent intent = new Intent(c, ViewProfile.class );
                //intent.putExtra("userID",""+items.get(i).getUserId());
                c.startActivity(intent);
            }
        });

    }

    public int getItemCount() {
        return items.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        ImageView userPicture;
        TextView userName;
        Button bProfile, bTrade;

        public ViewHolder (View v) {
            super(v);
            userPicture = (ImageView)v.findViewById(R.id.userpic);
            userName = (TextView) v.findViewById(R.id.username);
            bTrade = (Button) v.findViewById(R.id.tradebook);
            bProfile = (Button) v.findViewById(R.id.userprofile);
        }
    }
}
