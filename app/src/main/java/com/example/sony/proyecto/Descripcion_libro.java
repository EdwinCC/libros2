package com.example.sony.proyecto;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

public class Descripcion_libro extends AppCompatActivity {





    private String currentUser;
    private Button buttonSelected;
    private ConstraintLayout cl;
    private int buttonCount;
    private String bookNames[] = {"asuitableboy", "infernaldevices", "lifeofpi"};
    private NavigationView mnavegar;
    private DrawerLayout mDrawLayout;
    Button add;
    Button boton;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navegation);


        Toolbar mtoolbar=(Toolbar)findViewById(R.id.navegation);
        setSupportActionBar(mtoolbar);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_menu);//This description is commonly used for accessibility/screen readers when the Home action is enabled

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);//o set several display options at once, see the setDisplayOptions methods



        mnavegar =(NavigationView)findViewById(R.id.navegar);

        mDrawLayout= (DrawerLayout)findViewById(R.id.drawLayout) ;




        mnavegar.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {


                switch (item.getItemId()) {
                    case R.id.menu_seccion_1:

                        Toast.makeText(getApplicationContext(),"Editar Lista",Toast.LENGTH_SHORT).show();


                        break;
                    case R.id.menu_seccion_2:

                        Toast.makeText(getApplicationContext(),"Editar Perfil",Toast.LENGTH_SHORT).show();


                        break;
                    case R.id.menu_seccion_3:

                        Toast.makeText(getApplicationContext(),"About",Toast.LENGTH_SHORT).show();

                        break;
                    case R.id.menu_seccion_4:

                        Toast.makeText(getApplicationContext(),"Log out",Toast.LENGTH_SHORT).show();
                        break;

                }


                mDrawLayout.closeDrawers();
                return true;
            }
        });










        cl = (ConstraintLayout) findViewById(R.id.conlay);

        currentUser = "user1";
        Log.d("IdfindViewById", "" + findViewById(R.id.imageButton1));

        buttonCount = cl.getChildCount()-3;
        initButtons();


        /////////////////////////////////77


        add = (Button) findViewById(R.id.newLibro);
        add.setOnClickListener(new View.OnClickListener () {
                                   public void onClick(View view) {
                                       Intent intent = new Intent(getApplicationContext(),NewBook.class);
                                       intent.putExtra("userN",currentUser);
                                       startActivity(intent);



                                   }
                               }
        );

        /////////////////////////////////7


        boton= (Button)findViewById(R.id.noti);
        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), notificaciones.class);
                startActivity(intent);
            }



        });


    }


    public void initButtons() {
        int id;
        ImageButton[] imageButton = new ImageButton[buttonCount];
        for (int i = 0; i < buttonCount; i++) {
            id = getResources().getIdentifier("imageButton" + (i + 1), "id", getPackageName());
            imageButton[i] = (ImageButton) findViewById(id);

            String uri = "@drawable/" + bookNames[i];
            int resourceId = getResources().getIdentifier(uri, "drawable", getPackageName());
            imageButton[i].setImageResource(resourceId);
            imageButton[i].setTag(bookNames[i]);
            imageButton[i].setOnClickListener(new Listener());
        }
    }


    class Listener implements View.OnClickListener {
        public void onClick(View v) {
            String name = (String) v.getTag();
            Intent intent = new Intent(getApplicationContext(), BookDescription.class);

            Log.i("getTag",name);

            String message = name;
            intent.putExtra("title", message);
            intent.putExtra("user", currentUser);
            startActivity(intent);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_postlogin, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case android.R.id.home:
                mDrawLayout.openDrawer(GravityCompat.START);
                return true;

            case R.id.action_profile:
                Toast toast1 =
                        Toast.makeText(getApplicationContext(),
                                "Perfil", Toast.LENGTH_SHORT);

                toast1.show();
                return true;

            case R.id.action_buscar:
                Toast toast2 =
                        Toast.makeText(getApplicationContext(),
                                "Buscar", Toast.LENGTH_SHORT);

                toast2.show();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }




}
