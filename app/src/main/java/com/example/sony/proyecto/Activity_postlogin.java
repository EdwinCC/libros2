package com.example.sony.proyecto;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.Toast;

public class Activity_postlogin extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_activity_postlogin);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        //Animacion de fondo

        //ImageView img=(ImageView)findViewById(R.id.loadingview);
        ImageView img=(ImageView)findViewById(R.id.imgbackground);
        img.setBackgroundResource(R.drawable.gifentregalibros);


        AnimationDrawable frameAnimation=(AnimationDrawable)img.getBackground();
        frameAnimation.start();

        //Saludo bienvenida

        String saludologin=getIntent().getExtras().getString("saludo");
        String cuenta=getIntent().getExtras().getString("cuenta");
        saludo(saludologin+" "+cuenta);

    }

    /*
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu_main,menu);

        MenuItem item=menu.findItem(R.id.spinner);
        Spinner spinner = (Spinner) MenuItemCompat.getActionView(item);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getApplicationContext(), R.array.valores_spinner, R.layout.support_simple_spinner_dropdown_item);

        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);

        //ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,R.array.spinner_list_item_array, android.R.layout.simple_spinner_item);
        //adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        //spinner.setAdapter(adapter);

        return true;
    }
    */

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_postlogin, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_profile:
                //metodoEdit()
                Intent intent1 = new Intent(this, Profile.class);
                startActivity(intent1);
                return true;

            case R.id.action_buscar:
                //metodoEdit()
                Toast toast1 =
                        Toast.makeText(getApplicationContext(),
                                "Buscar", Toast.LENGTH_SHORT);

                toast1.show();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private void saludo(String txtsaludo){
        Toast tsaludo=new Toast(getApplicationContext());
        Toast.makeText(getApplicationContext(), txtsaludo, Toast.LENGTH_LONG);
    }

}
