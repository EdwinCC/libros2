package com.example.sony.proyecto;

public class Item1 {
    private String id;
    private int pic;

    public Item1(String uid, int upic) {
        id = uid;
        pic = upic;
    }

    public int getPic() {return pic;}
    public void setPic(int picId) {pic = picId;}
    public String getGenId() {return id;}
    public void setGenId(String uId) {id = uId;}

}
